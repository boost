<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<title>Integer Bit Mask Templates</title>
</head>

<body bgcolor="white" text="black" link="blue" vlink="purple" alink="red">
<h1><img src="../../../boost.png" alt="boost.png (6897 bytes)"
align="middle" width="277" height="86">Integer Bit Mask Templates</h1>

<p>The class templates in <cite><a
href="../../../boost/integer/integer_mask.hpp">&lt;boost/integer/integer_mask.hpp&gt;</a></cite>
provide bit masks for a certain bit position or a contiguous-bit pack of a
certain size.  The types of the masking constants come from the <a
href="../integer.htm">integer type selection templates</a> header.</p>

<h2><a name="contents">Contents</a></h2>

<ul>
	<li><a href="#contents">Contents</a></li>
	<li><a href="#synopsis">Synopsis</a></li>
	<li><a href="#single">Single Bit-Mask Class Template</a></li>
	<li><a href="#group">Group Bit-Mask Class Template</a></li>
	<li><a href="#mpl">MPL-Compatible Variants</a></li>
	<li><a href="#example">Example</a></li>
	<li><a href="#demo">Demonstration Program</a></li>
	<li><a href="#rationale">Rationale</a></li>
	<li><a href="#credits">Credits</a></li>
</ul>

<h2><a name="synopsis">Synopsis</a></h2>

<blockquote><pre>
#include &lt;<a href="../../../boost/integer_fwd.hpp">boost/integer_fwd.hpp</a>&gt;  <i>// forwarding header</i>
#include &lt;<a href="../../../boost/integer.hpp">boost/integer.hpp</a>&gt;      <i>// for boost::int_fast_t</i>
#include &lt;cstddef&gt;                <i>// for std::size_t</i>

namespace boost
{

// MPL-compatible
template &lt; int Offset &gt;
struct integer_hi_mask
{
    static  bool const  is_specialized = <em>implementation_supplied</em>;
    static  int const   bit_offset = Offset;

    typedef <em>implementation_supplied</em>  type;
    typedef <em>implementation_supplied</em>  value_type;
    static  value_type const  value = <em>implementation_supplied</em>;
    // There are other (optional) operations....
};

template &lt; int Length &gt;
struct integer_lo_mask
{
    static  bool const  is_specialized = <em>implementation_supplied</em>;
    static  int const   bit_count = Length;

    typedef <em>implementation_supplied</em>  type;
    typedef <em>implementation_supplied</em>  value_type;
    static  value_type const  value = <em>implementation_supplied</em>;
    // There are other (optional) operations....
};

// single
template &lt; std::size_t Bit &gt;
class high_bit_mask_t
{
public:
    typedef typename integer_hi_mask&lt;Bit&gt;::value_type  least;
    typedef int_fast_t&lt;least&gt;::fast                     fast;

    static const least  high_bit = integer_hi_mask&lt;Bit&gt;::value;
    static const fast   high_bit_fast = high_bit;

    static const std::size_t  bit_position = Bit;

};

// group
template &lt; std::size_t Bits &gt;
class low_bits_mask_t
{
public:
    typedef typename integer_lo_mask&lt;Bits&gt;::value_type  least;
    typedef int_fast_t&lt;least&gt;::fast                      fast;

    static const least  sig_bits = integer_lo_mask&lt;Bits&gt;::value;
    static const fast   sig_bits_fast = sig_bits;

    static const std::size_t  bit_count = Bits;

};

}  // namespace boost
</pre></blockquote>

<h2><a name="single">Single Bit-Mask Class Template</a></h2>

<p>The <code>boost::high_bit_mask_t</code> class template provides
constants for bit masks representing the bit at a certain position.  The
masks are equivalent to the value 2<sup><code>Bit</code></sup>, where
<code>Bit</code> is the template parameter.  The bit position must be a
nonnegative number from zero to <i>Max</i>, where <dfn>Max</dfn> is one
less than the number of bits supported by the largest unsigned built-in
integral type.  The following table describes the members of an
instantiation of <code>high_bit_mask_t</code>.</p>

<table border="1" cellpadding="5">
	<caption>Members of the <code>boost::high_bit_mask_t</code> Class
		Template</caption>
	<tr>
		<th>Member</th>
		<th>Meaning</th>
	</tr>
	<tr>
		<td><code>least</code></td>
		<td>The smallest unsigned built-in type that supports the given
			bit position.</td>
	</tr>
	<tr>
		<td><code>fast</code></td>
		<td>The quick-to-manipulate analog of <code>least</code>.</td>
	</tr>
	<tr>
		<td><code>high_bit</code></td>
		<td>A <code>least</code> constant of the desired bit-masking
			value.</td>
	</tr>
	<tr>
		<td><code>high_bit_fast</code></td>
		<td>A <code>fast</code> analog of <code>high_bit</code>.</td>
	</tr>
	<tr>
		<td><code>bit_position</code></td>
		<td>The value of the template parameter, in case its needed from
			a renamed instantiation of the class template.</td>
	</tr>
</table>

<h2><a name="group">Group Bit-Mask Class Template</a></h2>

<p>The <code>boost::low_bits_mask_t</code> class template provides
constants for bit masks representing the lowest bits of a certain
amount.  The masks are equivalent to the value
(2<sup><code>Bits</code></sup> - 1), where <code>Bits</code> is the
template parameter.  The bit amount must be a nonnegative number from
zero to <i>Max</i>, where <dfn>Max</dfn> is the number of bits supported
by the largest unsigned built-in integral type.  The following table
describes the members of an instantiation of
<code>low_bits_mask_t</code>.</p>

<table border="1" cellpadding="5">
	<caption>Members of the <code>boost::low_bits_mask_t</code> Class
		Template</caption>
	<tr>
		<th>Member</th>
		<th>Meaning</th>
	</tr>
	<tr>
		<td><code>least</code></td>
		<td>The smallest unsigned built-in type that supports the given
			bit count.</td>
	</tr>
	<tr>
		<td><code>fast</code></td>
		<td>The quick-to-manipulate analog of <code>least</code>.</td>
	</tr>
	<tr>
		<td><code>sig_bits</code></td>
		<td>A <code>least</code> constant of the desired bit-masking
			value.</td>
	</tr>
	<tr>
		<td><code>sig_bits_fast</code></td>
		<td>A <code>fast</code> analog of <code>sig_bits</code>.</td>
	</tr>
	<tr>
		<td><code>bit_count</code></td>
		<td>The value of the template parameter, in case its needed from
			a renamed instantiation of the class template.</td>
	</tr>
</table>

<h2><a name="mpl">MPL-Compatible Variants</a></h2>

<p>The single and group bit-mask class templates have several drawbacks:</p>

<ul>
	<li>You must know the valid bit-lengths in advance.</li>
	<li>Using an inappropriate parameter value results in a compiler
	    diagnostic.</li>
	<li>The type names used are inconsistent with other transformations in
	    Boost, like in <a href="../../mpl/">MPL</a>.</li>
	<li>The above two facts make use of the regular bit-mask class templates
	    incompatible with template meta-programming techniques.</li>
</ul>

<p>The <code>integer_hi_mask</code> and <code>integer_lo_mask</code> class
templates provide MPL-compatible alternatives.  These alternatives have the
form:</p>

<blockquote><pre>
template&lt; int <var>Size</var> &gt;
struct <var>name</var>
{
    static  bool const  is_specialized = <em>implementation_supplied</em>;
    static  int const   <var>switch_id</var> = <var>Size</var>;

    typedef <em>implementation_supplied</em>  type;
    typedef <em>implementation_supplied</em>  value_type;
    static  value_type const  value = <em>implementation_supplied</em>;
    // with other operations...
};
</pre></blockquote>

<p>Only some of the members are always present.  The presence of other members
and operations is flagged by the (always-present) <code>is_specialized</code>.</p>

<table border="2" cellpadding="5" align="center">
	<caption>Permanent Members of the MPL-Compatible Masking Class Template
	         Types</caption>
	<tr>
		<th>Class Template Member</th>
		<th>Meaning</th>
	</tr>
	<tr>
		<td><code>is_specialized</code></td>
		<td>Flag indicating when a particular template class instantiation is a
		    valid meta-function (<code>true</code>) or not (<code>false</code>).</td>
	</tr>
	<tr>
		<td><code><var>switch_id</var></code> (Actual name is template-specific.)</td>
		<td>The value of the main control parameter, accessible even if the
			template class instantiation is aliased.</td>
	</tr>
</table>

<p>The optional members are based from inheriting from a <a
href="../../mpl/doc/refmanual/integral-constant.html">MPL-style Integral
Constant</a> type, but only if <code>is_specialized</code> is <code>true</code>.</p>

<table border="2" cellpadding="5" align="center">
	<caption>Optional Members of the MPL-Compatible Masking Types</caption>
	<tr>
		<th>Class Template Member</th>
		<th>Meaning</th>
	</tr>
	<tr>
		<td><code>value</code></td>
		<td>The actual bit mask.</td>
	</tr>
	<tr>
		<td><code>value_type</code></td>
		<td>The type of the bit mask value.</td>
	</tr>
	<tr>
		<td><code>type</code></td>
		<td>The Integral Constant</a> implementation type, which should be
			<code><a href="../../mpl/doc/refmanual/integral-c.html">boost::mpl::
			integral_c</a>&lt; <var>value_type</var>, <var>value</var>
			&gt;</code>.</td>
	</tr>
</table>

<p>The Integral Constant prototype also adds the following operations:</p>

<table border="2" cellpadding="5" align="center">
	<caption>Optional Operations of the MPL-Compatible Masking Types</caption>
	<tr>
		<th>Operation (with <var>n</var> as a masking type)</th>
		<th>Meaning</th>
	</tr>
	<tr>
		<td><code>boost::mpl::next&lt; n &gt;::type</code></td>
		<td><code>boost::mpl::next&lt; n::type &gt;::type</code>, i.e.
			<code>boost::mpl::integral_c&lt; n::value_type, n::value + 1
			&gt;</code>.</td>
	</tr>
	<tr>
		<td><code>boost::mpl::prior&lt; n &gt;::type</code></td>
		<td><code>boost::mpl::prior&lt; n::type &gt;::type</code>, i.e.
			<code>boost::mpl::integral_c&lt; n::value_type, n::value - 1
			&gt;</code>.</td>
	</tr>
	<tr>
		<td><code>n::value_type const  c = n();</code></td>
		<td><var>c</var> is set to <code>n::value</code>.</td>
	</tr>
</table>

<p>The specifics for each masking class template are:</p>

<table border="2" cellpadding="5" align="center">
	<caption>Criteria for the MPL-Compatible Masking Types<br>
	         (Everything besides the parameter ID is in name-space
	         <code>boost</code> except where indicated.)</caption>
	<tr>
		<th>Class Template</th>
		<th>Parameter Member ID</th>
		<th>Classic Equivalent</th>
		<th>Value Type</th>
		<th>Value</th>
		<th>Valid Range</th>
	</tr>
	<tr>
		<td><code>integer_hi_mask</code></td>
		<td><code>bit_offset</code></td>
		<td><code>high_bit_mask_t</code></td>
		<td><code>sized_integral &lt; bit_offset + 1, unsigned &gt;</code></td>
		<td>2<sup><code>bit_offset</code></sup></td>
		<td><code>0 &lt;= bit_offset &lt; std::numeric_limits&lt; uintmax_t &gt;::digits</code></td>
	</tr>
	<tr>
		<td><code>integer_lo_mask</code></td>
		<td><code>bit_count</code></td>
		<td><code>low_bits_mask_t</code></td>
		<td><code>sized_integral &lt; bit_count, unsigned &gt;</code></td>
		<td>2<sup><code>bit_offset</code></sup> - 1</td>
		<td><code>0 &lt;= bit_count &lt;= std::numeric_limits&lt; uintmax_t &gt;::digits</code></td>
	</tr>
</table>

<h2><a name="example">Example</a></h2>

<blockquote><pre>
#include &lt;<a href="../../../boost/integer/integer_mask.hpp">boost/integer/integer_mask.hpp</a>&gt;

//...

int main()
{
    typedef boost::high_bit_mask_t&lt;29&gt;  mask1_type;
    typedef boost::low_bits_mask_t&lt;15&gt;  mask2_type;

    mask1_type::least  my_var1;
    mask2_type::fast   my_var2;
    //...

    my_var1 |= mask1_type::high_bit;
    my_var2 &amp;= mask2_type::sig_bits_fast;

    //...
}
</pre></blockquote>

<h2><a name="demo">Demonstration Program</a></h2>

<p>The program <a href="../test/integer_mask_test.cpp">integer_mask_test.cpp</a>
is a simplistic demonstration of the results from instantiating various
examples of the bit mask class templates.</p>

<h2><a name="rationale">Rationale</a></h2>

<p>The class templates in this header are an extension of the <a
href="../integer.htm">integer type selection class templates</a>.  The new
class templates provide the same sized types, but also convienent masks
to use when extracting the highest or all the significant bits when the
containing built-in type contains more bits.  This prevents
contaimination of values by the higher, unused bits.</p>

<h2><a name="credits">Credits</a></h2>

<p>The author of the Boost bit mask class templates is <a
href="http://www.boost.org/people/daryle_walker.html">Daryle Walker</a>.</p>

<hr>

<p>Revised July 29, 2008</p>

<p>&copy; Copyright Daryle Walker 2001.  Use, modification, and distribution are
subject to the Boost Software License, Version 1.0.  (See accompanying file <a
href="../../../LICENSE_1_0.txt">LICENSE_1_0.txt</a> or a copy at &lt;<a
href="http://www.boost.org/LICENSE_1_0.txt">http://www.boost.org/LICENSE_1_0.txt</a>&gt;.)</p>
</body>
</html>
