
.. Copyright Aleksey Gurtovoy, David Abrahams 2007.
.. Distributed under the Boost
.. Software License, Version 1.0. (See accompanying
.. file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
.. |Composition and Argument Binding| replace:: `Composition and Argument Binding`_

.. |composition| replace:: `composition <|Composition and Argument Binding link|>`__
.. |argument binding| replace:: `argument binding <|Composition and Argument Binding link|>`__
