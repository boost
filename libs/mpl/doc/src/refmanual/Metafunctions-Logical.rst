
.. Copyright Aleksey Gurtovoy, David Abrahams 2007.
.. Distributed under the Boost
.. Software License, Version 1.0. (See accompanying
.. file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

.. |logical| replace:: `logical <|Logical Operations|>`__
.. |Logical Operations| replace:: `Logical Operations`_
.. |logical operations| replace:: `logical operations`_
